#!/usr/bin/env python3

from urllib.request import urlopen
import re
import os

BASEURL="http://cinnamon-spices.linuxmint.com/applets"
PATTERN=re.compile('<a href="([^"]+)">([^<]+)</a>')
ZIPPAT=re.compile("<a href=\"(http://cinnamon-spices.linuxmint.com/uploads/applets/[^.]+.zip)\" class='small awesome'>Download</a>")

def get_applets(url):
	p = urlopen(url).read().decode("utf8")
	matches = PATTERN.findall(p)
	for m in matches:
		url = m[0]
		if url.startswith(BASEURL+"/"):
			yield((url, m[1]))

def download(url, name):
	p = urlopen(url).read().decode("utf8")
	match = ZIPPAT.search(p)
	zipurl = match.group(1)
	short = shortname(name)
	os.system("curl -R %s -o SOURCES/%s.zip" % (zipurl,short))
	os.system("touch SPECS/cinnamon-applet-%s.spec" % (short))

def shortname(long):
	for c in " +/\\_":
		long = long.replace(c, "-")
	for c in "()[]{}&,.":
		long = long.replace(c, "")
	long = long.replace("--", "-")
	while long.endswith("-"):
		long = long[:-1]
	return long.lower()

for url,name in get_applets(BASEURL):
	download (url, name)
