Name:		cinnamon-extra-applets
Version:	1
Release:	1%{?dist}
Summary:	Extra applets for the cinnamon desktop
License:	GPLv2+
URL:		http://cinnamon-spices.linuxmint.com/applets
Source0:	%{name}-%{version}.tar.bz2
Requires:	cinnamon
Requires:	libgtop2
BuildArchitectures: noarch

%description
A collection of applet packages for Cinnamon.
Included applets:
- Weather 1.7.3
- Multi-Core System Monitor 1.4
- Places 1.2
- Better Settings 1.1.1

%prep
%setup -q
%build
%install
mkdir -p %{buildroot}/usr/share/glib-2.0/schemas
cp *.gschema.xml %{buildroot}/usr/share/glib-2.0/schemas
mkdir -p %{buildroot}/usr/share/cinnamon/applets
cp -r * %{buildroot}/usr/share/cinnamon/applets

%files
%defattr(-,root,root)
/usr/share/cinnamon/applets/*

%doc

%changelog
* Wed Mar 20 2013 Andreas Zwinkau <qznc@web.de> - 1-1
- Initial package
