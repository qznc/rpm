#%define __build_pr_downloader			1

# Few notes about licensing:
# The main game is GPLv2+
# - AI/Global/RAI/README says that AI is licensed under GPLv3+
# - License for some sounds is LGPLv2 as taken from bzflag.
# - License for some graphics is GFDL and (GFDL or CC-BY). See
#	installer/builddata/bitmaps/README.txt.
Name:			spring
Version:		94.1
Release:		1%{?dist}
Summary:		Multiplayer, 3D realtime strategy combat game
Group:			Amusements/Games
License:		GPLv2+ and GPLv3+ and LGPLv2 and GFDL and (GFDL or CC-BY)
URL:			http://springrts.com

Source0:		http://downloads.sourceforge.net/project/springrts/springrts/spring-%{version}/spring_%{version}_src.tar.lzma
Source1:		spring-README.Fedora

Patch1:			spring-89-assimp-remove.patch

BuildRoot:		%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	cmake SDL-devel openal-soft-devel boost-devel DevIL-devel
BuildRequires:	glew-devel libvorbis-devel DevIL-ILUT-devel
BuildRequires:	freetype-devel python-devel allegro-devel zip
BuildRequires:	java-devel-openjdk p7zip-plugins
BuildRequires:	desktop-file-utils
BuildRequires:	assimp-devel
BuildRequires:	libxslt asciidoc docbook-style-xsl
Requires:		spring-lobby spring-installer spring-maps-default fluidsynth dumb-devel

Provides:		spring-dedicated = %{version}-%{release}
Provides:		bundled(lua) = 5.1.4
Provides:		bundled(md5-deutsch)

# Spring PPC version is still experimental.
# Exclude for now.
ExcludeArch:	ppc ppc64


%description
Spring is a project aiming to create a new, versatile, full 3D Real Time
Strategy Engine.
Spring is designed to be played as online multiplayer matches, but some AI are
also available to play against the computer.
Please read the README.Fedora file to get started. The Spring wiki is also a
great resource, read it here: http://spring.clan-sy.com/wiki/Main_Page


%if %{?__build_pr_downloader}0 == 1

%package pr-downloader
Summary:		Console game / map downloader for the Spring engine.
Group:			Amusements/Games


%description pr-downloader
pr-downloader is a console cross-platform download tool for spring.
It can download games and maps supporting currently all main download systems,
including rapid, plasma and upq. 


%package pr-downloader-devel
Summary:		Development files for the console game / map downloader for the Spring engine.
Group:			Amusements/Games


%description pr-downloader-devel
C-API files for the pr-downloader console game / map downloader for the Spring engine.

%endif


%prep
%setup -q -n %{name}_%{version}
%patch1 -p0 -b .spring-89-assimp-remove

cp -p %{SOURCE1} README.Fedora
touch ./rts/build/cmake/FindAllegro.cmake
find rts/lib/7z -type f | xargs chmod -x 


%build
%cmake -DBUILD_SHARED_LIBS:BOOL=OFF \
		-DDATADIR:PATH=share/%{name} \
		-DLIBDIR:PATH=%{_lib} \
		.
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT

make install DESTDIR=$RPM_BUILD_ROOT

# Prepare maps and mods dirs
mkdir -p $RPM_BUILD_ROOT%{_datadir}/%{name}/{maps,mods}

# Move icons into proper Freedesktop hicolor theme
mkdir -p $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/48x48/apps
mv $RPM_BUILD_ROOT%{_datadir}/pixmaps/%{name}.png \
	$RPM_BUILD_ROOT%{_datadir}/icons/hicolor/48x48/apps/
mkdir -p $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/48x48/mimetypes/
mv $RPM_BUILD_ROOT%{_datadir}/pixmaps/application-x-spring-demo.png \
		 $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/48x48/mimetypes/

# Remove redundant documents.
rm -rf $RPM_BUILD_ROOT%{_docdir}/spring-VERSION

# Fix Icon entry
sed -i -e 's/^Icon=\(.*\).png/Icon=\1/g' \
		$RPM_BUILD_ROOT%{_datadir}/applications/%{name}.desktop
desktop-file-install	\
		--dir $RPM_BUILD_ROOT%{_datadir}/applications \
		--remove-category Application \
		--delete-original \
		$RPM_BUILD_ROOT%{_datadir}/applications/%{name}.desktop

%if %{?__build_pr_downloader}0 != 1

# Remove redundant pr_downloader files.
rm -rf $RPM_BUILD_ROOT/%{_includedir}/spring/Downloader/pr-downloader.h
rm -rf $RPM_BUILD_ROOT/%{_libdir}/libpr-downloader_shared.so
rm -rf $RPM_BUILD_ROOT/%{_libdir}/libpr-downloader_static.a
rm -rf $RPM_BUILD_ROOT/%{_libdir}/pkgconfig/libspringdownloader.pc

%endif


%post
# Icons
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ] ; then
	%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi
# MimeType entry in the desktop file
update-desktop-database &> /dev/null || :
# MIME package
update-mime-database %{_datadir}/mime &> /dev/null || :


%postun
# Icons
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ] ; then
	%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi
# MimeType entry in the desktop file
update-desktop-database &> /dev/null || :
# MIME package
update-mime-database %{_datadir}/mime &> /dev/null || :


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc AUTHORS LICENSE README.Fedora THANKS FAQ README.markdown gpl-2.0.txt gpl-3.0.txt
%{_bindir}/*
%{_libdir}/libunitsync.so
%{_libdir}/libspringserver.so
%{_datadir}/mime/packages/%{name}.xml
%{_datadir}/icons/hicolor/48x48/*/*.png
%{_datadir}/applications/*.desktop
%{_datadir}/%{name}
%{_mandir}/man6/*


%if %{?__build_pr_downloader}0 == 1

%files pr-downloader
%{_libdir}/libpr-downloader_shared.so


%files pr-downloader-devel
%{_libdir}/pkgconfig/libspringdownloader.pc
%{_libdir}/libpr-downloader_static.a
%{_includedir}/spring/Downloader/pr-downloader.h

%endif


%changelog
* Tue Mar 26 2013 andi - 94.1-1
- Upstream update

* Sat Feb 09 2013 Denis Arnaud <denis.arnaud_fedora@m4x.org> - 91.0-3
- Rebuild for Boost-1.53.0

* Thu Dec 13 2012 Adam Jackson <ajax@redhat.com> - 91.0-2
- Rebuild for glew 1.9.0

* Sun Sep 09 2012 Gilboa Davara <gilboad [AT] gmail [DOT] com> - 91.0-1
- 91.0: Urgent upstream bugfix release.

* Sat Aug 12 2012 Gilboa Davara <gilboad [AT] gmail [DOT] com> - 90.0-1
- 90.0: Upstream release.
- Drop main boost TIME_UTC_ patch.
- ... But add E323AI boost TIME_UTC_ patch...
- pr-downloader still disabled.

* Sat Aug 12 2012 Gilboa Davara <gilboad [AT] gmail [DOT] com> - 89.0-5
- Temporary disable pr-downloader to resolve build error.

* Thu Aug 09 2012 Orion Poplawski <orion@nwra.com> - 89.0-4
- Add upstream patch to use new boost TIME_UTC_ symbol

* Wed Aug 01 2012 Adam Jackson <ajax@redhat.com> - 89.0-3
- Rebuild for new glew

* Sat Jul 21 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 89.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Mon Jul 16 2012 Gilboa Davara <gilboad [AT] gmail [DOT] com> - 89.0-2
- Switch to Fedora's built in assimp.
- Per-#505636, bundled md5 implementation description is now deutsch instead of Aladdin.
- Minor spec cleanup.

* Mon Jul 16 2012 Gilboa Davara <gilboad [AT] gmail [DOT] com> - 89.0-1
- 89.0: Upstream bugfix release.
- pr-downloader added, moved to a separate package.
- Updated dso patch.
- Drop the gcc 4.7 patch (fixed upstream).

* Wed Apr  4 2012 Tom Callaway <spot@fedoraproject.org> - 88.0-2
- fix compile with gcc 4.7

* Sat Mar 24 2012 Gilboa Davara <gilboad [AT] gmail [DOT] com> - 88.0-1
- 88.0: Upstream bugfix release.

* Mon Mar 07 2012 Gilboa Davara <gilboad [AT] gmail [DOT] com> - 87.0-1
- 87.0: Upstream bugfix release.

* Mon Feb 27 2012 Gilboa Davara <gilboad [AT] gmail [DOT] com> - 86.0-1
- 86.0: Upstream feature / bugfix release.

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 84.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Tue Dec 24 2011 Gilboa Davara <gilboad [AT] gmail [DOT] com> - 85.0-1
- 85.0: Upstream bug fix / feature release.

* Tue Nov 22 2011 Gilboa Davara <gilboad [AT] gmail [DOT] com> - 84.0-1
- 84:.0: Urgent upstream bug fix release.

* Sat Nov 19 2011 Gilboa Davara <gilboad [AT] gmail [DOT] com> - 83.0-2
- Static lib 'provides' tags.

* Tue Nov 7 2011 Gilboa Davara <gilboad [AT] gmail [DOT] com> - 83.0-1
- New upstream release.

* Tue Aug 22 2011 Gilboa Davara <gilboad [AT] gmail [DOT] com> - 0.82.7.1-6
- Java/F16 compile fix.

* Tue Jul 26 2011 Bruno Wolff III <bruno@wolff.to> - 0.82.7.1-5
- Rebuild for boost 1.47 soname bump

* Mon Jun 20 2011 ajax@redhat.com - 0.82.7.1-4
- Rebuild for new glew soname

* Thu Mar 31 2011 Gilboa Davara <gilboad [AT] gmail [DOT] com> - 0.82.7.1-3
- Boost fix (F15/rawhide).
- GCC 4.6 (F15) compilation fixes.

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.82.7.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sun Jan 9 2011 Gilboa Davara <gilboad [at] gmail [dot] com> -	0.82.7.1-1
- New upstream version: 0.82.7.1. (Bug fixes: http://springrts.com/phpbb/viewtopic.php?f=2&t=25038)

* Tue Jan 4 2011 Gilboa Davara <gilboad [at] gmail [dot] com> -	0.82.7-1
- New upstream version: 0.82.7. (Bug fixes: http://springrts.com/phpbb/viewtopic.php?t=24970)
- Include patch applied upstream.

* Tue Nov 18 2010 Gilboa Davara <gilboad [at] gmail [dot] com> -	0.82.6.1-1
- version 0.82.6.1 (bugfixes, http://springrts.com/phpbb/viewtopic.php?t=24318)

* Tue Sep 07 2010 Aurelien Bompard <abompard@fedoraproject.org> -	0.82.5.1-1
- version 0.82.5.1 (bugfixes, http://springrts.com/phpbb/viewtopic.php?t=23955)

* Sun Sep 05 2010 Aurelien Bompard <abompard@fedoraproject.org> -	0.82.5-1
- version 0.82.5 (bugfixes: http://springrts.com/phpbb/viewtopic.php?t=23921)

* Mon Aug 16 2010 Aurelien Bompard <abompard@fedoraproject.org> -	0.82.3-1
- version 0.82.3 (details here: http://springrts.com/phpbb/viewforum.php?f=2)

* Sun May 02 2010 Bruno Wolff III <bruno@wolff.to> - 0.81.2.1-2
- Fix DSO linking issue with dlopen - bz565103
- Package man pages

* Sun Mar 21 2010 Aurelien Bompard <abompard@fedoraproject.org> -	0.81.2.1-1
- New release: 0.81.2.1 (bugfix)

* Tue Feb 23 2010 Aurelien Bompard <abompard@fedoraproject.org> -	0.81.2-1
- New release: 0.81.2

* Sat Feb 06 2010 Aurelien Bompard <abompard@fedoraproject.org> -	0.81.1.3-1
- New release: 0.81.1.3
- drop patches 1 & 2 (merged upstream)

* Sun Jan 24 2010 Aurelien Bompard <abompard@fedoraproject.org> -	0.80.5.2-3
- fix build on rawhide

* Fri Jan 22 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 0.80.5.2-2
- Rebuild for Boost soname bump

* Sat Nov 21 2009 Aurelien Bompard <abompard@fedoraproject.org> -	0.80.5.2-1
- New release: 0.80.5.2

* Sun Sep 20 2009 Aurelien Bompard <abompard@fedoraproject.org> -	0.80.4.2-1
- New release: 0.80.4.2 (bugfix)

* Sat Sep 05 2009 Aurelien Bompard <abompard@fedoraproject.org> -	0.80.4.1-1
- New release: 0.80.4.1 (bugfix)

* Thu Sep 03 2009 Aurelien Bompard <abompard@fedoraproject.org> -	0.80.4-1
- New release: 0.80.4

* Sat Aug 22 2009 Aurelien Bompard <abompard@fedoraproject.org> -	0.80.2-1
- New release: 0.80.2

* Wed Aug 12 2009 Ville Skyttä <ville.skytta@iki.fi> - 0.79.1.2-4
- Use lzma compressed upstream tarball.

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.79.1.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sun Jul 19 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.79.1.2-2
- use OpenJDK's version of Java

* Sat Jul 18 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.79.1.2-1
- version 0.79.1.2
- remove obsolete fonts hack
- build dedicated server

* Sat May 23 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.79.0.2-1
- version 0.79.0.2
- update URL

* Sun Mar 22 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.78.2.1-9
- exclude KAI, since it's deprecated
 (http://spring.clan-sy.com/phpbb/viewtopic.php?f=20&t=18196)

* Tue Mar 17 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.78.2.1-8
- use relative symlinks for the fonts
- fix perms in the 7zip sources
- handle the font package rename in F11

* Sun Mar 15 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.78.2.1-7
- add info about licensing
- use system fonts

* Mon Feb 02 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.78.2.1-6
- use cmake, as requested by upstream:
	http://spring.clan-sy.com/phpbb/viewtopic.php?p=330802#p330802

* Mon Feb 02 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.78.2.1-5
- use Fedora's compilation flags

* Sun Feb 01 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.78.2.1-4
- fix x86_64 build

* Sun Jan 18 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.78.2.1-2
- remove trademarks from summary and description
- add patch for python 2.6
- fix detection of devil libraries

* Sun Jan 18 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.78.2.1-1
- update to 0.78.2.1 (bugfix release)

* Sun Jan 18 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.78.1.1-2
- Other changes from the review:
- remove Cmake-specific patch
- drop meta-package

* Sat Jan 17 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.78.1.1-1
- Implement changes from the review (#478767):
- improve description
- add a README.Fedora file
- remove commas from (build)requires
- remove vendor from the desktop file
- update to version 0.78.1.1

* Sun Jul 09 2006 Aurelien Bompard <abompard@fedoraproject.org> 0.77-1.b5
- initial package
