Name:			springlobby
Version:		0.169
Release:		1%{?dist}
Summary:		A lobby client for the spring RTS game engine

Group:			Amusements/Games
# License clarification: http://springlobby.info/issues/show/810
License:		GPLv2
URL:			http://springlobby.info
Source0:		http://www.springlobby.info/tarballs/springlobby-%{version}.tar.bz2

BuildRequires:	cmake
BuildRequires:	wxGTK-devel, rb_libtorrent-devel
BuildRequires:	SDL-devel, SDL_sound-devel, SDL_mixer-devel
BuildRequires:	pkgconfig, desktop-file-utils, gettext
BuildRequires:	openal-devel, libcurl-devel
BuildRequires:	alure-devel, libnotify-devel

# There are other "lobbies" for spring, make a virtual-provides
Provides:		spring-lobby = %{version}-%{release}

Requires:		hicolor-icon-theme
# Springlobby is completely useless without the spring package
Requires:		spring
# Spring does not build on PPC, exclude it here too
ExcludeArch:	ppc ppc64

%description
SpringLobby is a free cross-platform lobby client for the Spring RTS project.

%prep
%setup -q

%build
# Use boost filesystem 2 explicitly (bug 654807)
%if 0%{?fedora} >= 18
export CFLAGS="$CFLAGS -DBOOST_FILESYSTEM_VERSION=3"
export CXXFLAGS="$CXXFLAGS -DBOOST_FILESYSTEM_VERSION=3"
%else
export CFLAGS="$CFLAGS -DBOOST_FILESYSTEM_VERSION=2"
export CXXFLAGS="$CXXFLAGS -DBOOST_FILESYSTEM_VERSION=2"
%endif
%cmake
make %{?_smp_mflags}

%install
%make_install

# install extra libraries
mkdir -p $RPM_BUILD_ROOT%{_libdir}
install src/downloader/lib/src/FileSystem/libFileSystem.so $RPM_BUILD_ROOT%{_libdir}/
install src/downloader/lib/src/libCurlWrapper.so $RPM_BUILD_ROOT%{_libdir}/

# Handled in %%doc
rm -rf $RPM_BUILD_ROOT%{_datadir}/doc/
# Useless file
rm -f $RPM_BUILD_ROOT%{_prefix}/config.h

# Fix Icon entry
sed -i -e 's/^Icon=\(.*\).svg/Icon=\1/g' \
		$RPM_BUILD_ROOT%{_datadir}/applications/%{name}.desktop
desktop-file-install	\
	--dir $RPM_BUILD_ROOT%{_datadir}/applications \
	--remove-category Application \
	--delete-original \
	$RPM_BUILD_ROOT%{_datadir}/applications/%{name}.desktop

%find_lang %{name}

%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
	touch --no-create %{_datadir}/icons/hicolor &>/dev/null
	gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -f %{name}.lang
%doc AUTHORS NEWS README COPYING THANKS
%{_bindir}/*
%{_libdir}/*
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/scalable/apps/*.svg

%changelog
* Tue Mar 26 2013 Andreas Zwinkau <qznc@web.de> - 0.169-1
- Update to upstream version 0.169

* Sun Feb 24 2013 Rahul Sundaram <sundaram@fedoraproject.org> - 0.147-4
- Rebuild for rb_libtorrent soname bump
- Clean up spec to follow current guidelines

* Sat Feb 09 2013 Denis Arnaud <denis.arnaud_fedora@m4x.org> - 0.147-3
- Rebuild for Boost-1.53.0

* Sun Feb 03 2013 Kevin Fenzi <kevin@scrye.com> - 0.147-2
- Rebuild for broken deps in rawhide

* Fri Jun 15 2012 Gilboa Davara <gilboad [AT] gmail [DOT] com> - 0.147-1
- Version 0.147 (Large number of fixes).

* Fri Mar 16 2012 Gilboa Davara <gilboad [AT] gmail [DOT] com> - 0.144-1
- Version 0.144 (Large number of fixes).

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.139-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Thu Nov 7 2011 Gilboa Davara <gilboad [AT] gmail [DOT] com> - 0.139-1
- Version 0.139 (Large number of fixes).

* Thu Aug 22 2011 Gilboa Davara <gilboad [AT] gmail [DOT] com> - 0.136-1
- Version 0.136 (Large number of fixes).

* Thu May 18 2011 Gilboa Davara <gilboad [AT] gmail [DOT] com> - 0.131-1
- Version 0.131 (Large number of fixes).

* Thu Mar 31 2011 Gilboa Davara <gilboad [AT] gmail [DOT] com> - 0.128-1
- Version 0.128 (Large number of fixes).
- libnotify patched dropped, handled up-stream.

* Thu Mar 10 2011 Gilboa Davara <gilboad [AT] gmail [DOT] com> - 0.124-1
- Version 0.124 (Large number of fixes).

* Fri Feb 14 2011 Leigh Scott <leigh123linux@googlemail.com> - 0.120-3
- specify boost_filesystem version
- patch for libnotify changes

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.120-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Tue Jan 11 2011 Gilboa Davara <gilboad [at] gmail [dot] com> - 0.120-1
- BT download broken by new spring release.

* Tue Dec 17 2010 Gilboa Davara <gilboad [at] gmail [dot] com> - 0.118-1
- version 0.118 (w/ GTK fix)
- BT download should work now.

* Tue Nov 18 2010 Gilboa Davara <gilboad [at] gmail [dot] com> - 0.116-1
- version 0.116 (w/ GTK fix)

* Wed Sep 29 2010 jkeating - 0.101-2
- Rebuilt for gcc bug 634757

* Wed Sep 15 2010 Aurelien Bompard <abompard@fedoraproject.org> - 0.101-1
- version 0.101

* Mon Aug 16 2010 Aurelien Bompard <abompard@fedoraproject.org> - 0.80-2
- version 0.95

* Wed Jul 14 2010 Dan Horák <dan@danny.cz> - 0.80-2
- rebuilt against wxGTK-2.8.11-2

* Mon May 10 2010 Aurelien Bompard <abompard@fedoraproject.org> - 0.80-1
- version 0.80

* Tue May 04 2010 Aurelien Bompard <abompard@fedoraproject.org> - 0.79-1
- version 0.79
- add patch to fix DSO linking
	(http://fedoraproject.org/wiki/UnderstandingDSOLinkChange)

* Sun May 02 2010 Aurelien Bompard <abompard@fedoraproject.org> - 0.78-1
- version 0.78

* Sun Mar 21 2010 Aurelien Bompard <abompard@fedoraproject.org> - 0.63-1
- version 0.63

* Sun Jan 31 2010 Aurelien Bompard <abompard@fedoraproject.org> - 0.61-2
- missing BR: openal-devel

* Sun Jan 31 2010 Aurelien Bompard <abompard@fedoraproject.org> - 0.61-1
- version 0.61

* Sat Jan 23 2010 Caolán McNamara <caolanm@redhat.com> - 0.40-2
- rebuild for boost

* Sun Dec 06 2009 Aurelien Bompard <abompard@fedoraproject.org> - 0.40-1
- version 0.40

* Mon Nov 09 2009 Aurelien Bompard <abompard@fedoraproject.org> - 0.35-1
- version 0.35

* Sun Oct 11 2009 Aurelien Bompard <abompard@fedoraproject.org> - 0.27-1
- version 0.27 

* Mon Sep 14 2009 Aurelien Bompard <abompard@fedoraproject.org> - 0.23-1
- version 0.23

* Wed Sep 09 2009 Aurelien Bompard <abompard@fedoraproject.org> - 0.22-1
- version 0.22

* Thu Aug 27 2009 Tomas Mraz <tmraz@redhat.com> - 0.19-2
- rebuilt with new openssl

* Sun Aug 23 2009 Aurelien Bompard <abompard@fedoraproject.org> - 0.19-1
- version 0.19

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sat Jul 18 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.3
- version 0.3

* Fri May 22 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.0.1.10461-1
- version 10461
- drop patch0 (merged upstream)

* Sun May 10 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.0.1.10425-5
- rebuild

* Tue Apr 28 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.0.1.10425-4
- rebuild

* Sun Mar 29 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.0.1.10425-3
- add patch for gcc 4.4

* Sat Mar 28 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.0.1.10425-2
- rebuild

* Sun Mar 22 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.0.1.10425-1
- revert to 10425

* Mon Mar 16 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.0.1.10429-4
- fix license tag

* Mon Mar 16 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.0.1.10429-3
- new scriptlets for the icon cache
- require hicolor-icon-theme

* Sun Mar 15 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.0.1.10429-2
- drop the version in the buildrequires
- don't package ChangeLog as %%doc

* Sun Mar 15 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.0.1.10429-1
- version 10429

* Wed Feb 25 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.0.1.10425-1
- version 10425
- drop both patches (merged upstream)

* Tue Feb 03 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.0.1.10393-1
- version 10393
- add patch to detect libunitsync.so properly
- drop workaround for rhbz#478589
- require spring
- add patch to fix gettext detection on x86_64

* Sat Jan 17 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.0.1.10387-1
- version 10387
- remove vendor from the desktop file

* Thu Jan 01 2009 Aurelien Bompard <abompard@fedoraproject.org> 0.0.1.10372-1
- initial package
