APPLETURL=http://cinnamon-spices.linuxmint.com/uploads/applets/
APPLETS=GVOU-I2IS-DJV7 Y0AY-WGJM-4CQJ FTBL-QMQO-FG7Q 62G3-RSBX-58HS
PKGNAME=cinnamon-extra-applets
PKGVER=1
FULLNAME=${PKGNAME}-${PKGVER}

SOURCES/$(PKGNAME)-$(PKGVER).tar.bz2:
	mkdir -p SOURCES/$(FULLNAME)
	cd SOURCES/$(FULLNAME) &&\
		$(foreach x,$(APPLETS),wget -c ${APPLETURL}$x.zip && unzip $x.zip && rm $x.zip;)
	cd SOURCES &&\
		tar -cjf $(FULLNAME).tar.bz2 $(FULLNAME)

.PHONY: clean

clean:
	rm -f  SOURCES/$(FULLNAME).tar.bz2
	rm -rf SOURCES/$(PKGNAME)

